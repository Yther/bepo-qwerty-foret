En arrivant à la lisière de la forêt, les deux animaux retrouvèrent vite la sorcière et le fils
des maîtres qui discutaient en buvant du thé. La femme sourit en les voyant.

« En voilà deux qui ont goûté à la magie.
– Figure toi qu'on en a bavé, c'est pas de tout repos de partager sa tête avec un chien.
– Elle a raison ! »

La sorcière agita son poignet et les deux animaux retrouvèrent le calme de leurs pensées.

« Voilà C'est rétablit. J'espère que vous arrêterez de vous feuler et aboyer dessus quand vous vous
croiserez maintenant ! »
